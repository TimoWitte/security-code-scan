package plugin

import (
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

// Match checks if the filename has a .sln, .csproj, .vbproj extension.
func Match(path string, info os.FileInfo) (bool, error) {
	// match .sln for root of multiprojects
	// match .csproj or .vbproj for the root of single projects
	switch filepath.Ext(info.Name()) {
	case ".sln", ".csproj", ".vbproj":
		return true, nil
	default:
		return false, nil
	}
}

func init() {
	plugin.Register("security-code-scan", Match)
}
